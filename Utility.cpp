#ifndef UTILITY_H
#define UTILITY_H


#include <string>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <vector>
#include <regex>

#include "Utility.h"

using namespace std;

Utility::Utility(){
}

Utility::~Utility(){
}

string Utility::trim(string str)
{
    size_t first = str.find_first_not_of(' ');
    size_t last = str.find_last_not_of(' ');
    if(first == last)
      return "";
    return str.substr(first, (last-first+1));
}

// split string into vector array by delim
vector<string> Utility::split(string str, char delimiter) {
  vector<string> internal;
  // Turn the string into a stream.
  stringstream ss(str); 
  string tok;

  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  return internal;
}



string Utility::removeLineBreak(string targetString)
{
  string::size_type pos = 0; // Must initialize
  while(( pos = targetString.find ("\r",pos)) != string::npos) {
    targetString.erase(pos, 1);
  }

  pos = 0;
  while ((pos = targetString.find ("\n",pos)) != string::npos) {
    targetString.erase(pos, 1);
  }
  return targetString;
}


vector<string> Utility::splitInstruction(string commandString){

  commandString = trim(commandString);
  regex e("\\s+");
  commandString = regex_replace(commandString, e, " ");
  commandString = removeLineBreak(commandString);
  return split(commandString, ' ');
}


// check a instruction is pipe or not.
// return the number appended the pipe or -1 when common instruction.
int Utility::checkOperatorNumber(string inputString)
{
  int pipeNumber = atoi(inputString.substr(1).c_str());
  return pipeNumber;
}


int Utility::checkInputType(string inputString)
{
  string firstString = inputString.size() > 0 ? inputString.substr(0, 1) : "";
  // cout << "check input type: " << inputString << ":" << firstString << endl;
  if(firstString == "|"){
    return 1;
  } else if(firstString == "!") {
    return 2;
  } else if((firstString == ">") && (inputString.size() == 1)) {
    return 3;
  } else if((firstString == "<") && (inputString.size() > 1)) {
    return 4;
  } else if((firstString == ">") && (inputString.size() > 1)) {
    return 5;
  } else if(firstString == "") {
    return -1;
  } else {
    return 0;
  }
}

int Utility::checkCommandType(string commandString)
{
  vector<string> instructionVector = splitInstruction(commandString);
  if(instructionVector.size() == 0)
    return -1;

  string firstInst = instructionVector[0];
  if(firstInst == "who")
    return 1;
  else if(firstInst == "tell")
    return 2;
  else if(firstInst == "yell")
    return 3;
  else if(firstInst == "name")
    return 4;
  else
    return 0;
}

#endif
