all: server.cpp noop number removetag removetag0
	g++ -std=c++11 -o server server.cpp
	g++ -std=c++11 -o singleProcessServer singleProcessServer.cpp
PipeHandler.o: PipeHandler.cpp
	g++ -c PipeHandler.cpp Pipe.o
Pipe.o: Pipe.cpp
	g++ -c Pipe.cpp
noop: ./ras/bin/noop.cpp
	g++ -o ./ras/bin/noop ./ras/bin/noop.cpp
number: ./ras/bin/number
	g++ -o ./ras/bin/number ./ras/bin/number.cpp
removetag: ./ras/bin/removetag.cpp
	g++ -o ./ras/bin/removetag ./ras/bin/removetag.cpp
removetag0: ./ras/bin/removetag.cpp
	g++ -o ./ras/bin/removetag0 ./ras/bin/removetag0.cpp
