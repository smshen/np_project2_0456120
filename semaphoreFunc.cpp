#ifndef SEMAPHOREFUNC_H
#define SEMAPHOREFUNC_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#define PREMS 0666
#define BIGCOUNT 10000

static struct sembuf op_lock[2] = {
  2, 0, 0,
  2, 1, SEM_UNDO
};

static struct sembuf op_unlock[1] = {
  2, -1, (IPC_NOWAIT | SEM_UNDO)
};

static struct sembuf op_endcreate[2] = {
  1, -1, SEM_UNDO,
  2, -1, SEM_UNDO
};

static struct sembuf op_open[1] = {
  1, -1, SEM_UNDO
};

static struct sembuf op_close[3] = {
  2, 0, 0,
  2, 1, SEM_UNDO,
  1, 1, SEM_UNDO
};


static struct sembuf op_op[1] = {
  0, 99, SEM_UNDO 
};

union semun {
  int    val;
  struct semid_ds *buff;
  ushort *array;
} semctl_arg;

int sem_create(key_t, int);
void sem_rm(int);
int sem_open(key_t);
void sem_close(int);
void sem_op(int, int);
void sem_wait(int);
void sem_signal(int);
void err_sys(const char*); 



void err_sys(const char* x) 
{ 
  perror(x); 
  // exit(1); 
}

int sem_create(key_t key, int initval)
{
  register int id, semval;
  // not intended for private semaphore
  if(key == IPC_PRIVATE) return(-1);
  else if(key == (key_t) -1) return(-1);

again:
  if((id = semget(key, 3, 0666 | IPC_CREAT)) < 0) return(-1);
  if(semop(id, &op_lock[0], 2) < 0){
    if(errno == EINVAL) goto again;
    err_sys("can't lock");
  }

  if((semval = semctl(id, 1, GETVAL, 0)) < 0) err_sys("can't GETVEL");
  if(semval == 0){
    semctl_arg.val = initval;
    if(semctl(id, 0, SETVAL, semctl_arg) < 0)
      err_sys("can't SETVAL[0]");
    semctl_arg.val = BIGCOUNT;
    if(semctl(id, 1, SETVAL, semctl_arg) < 0)
      err_sys("can't SETVAL[1]");
  }
  if(semop(id, &op_endcreate[0], 2) < 0)
    err_sys("can't end create");

  return id;

}

void sem_rm(int id)
{
  if(semctl(id, 0, IPC_RMID, 0) < 0)
    err_sys("can't IPC_RMID");
}

int sem_open(key_t key)
{
  register int id;
  if(key == IPC_PRIVATE) return(-1);
  else if(key == (key_t) -1) return(-1);

  if((id = semget(key, 3, 0)) < 0) return(-1);

  // Decrement the process counter. We don't need the lock to  do this
  if(semop(id, &op_open[0], 1) < 0) err_sys("can't open");

  return id;
}

void sem_close(int id)
{
  register int semval;
  // The follow semop() first get a lock on the semaphore
  // then increments [1] - the process counter.
  if(semop(id, &op_close[0], 3) < 0) err_sys("can't semop");
  
  // if this is the last reference to the semaphore, remove this.
  if((semval = semctl(id, 1, GETVAL, 0)) < 0) err_sys("can't GETVAL");
  if(semval > BIGCOUNT) err_sys("sem[1] > BIGCOUNT");
  else if(semval == BIGCOUNT) sem_rm(id);
  else
    if(semop(id, &op_unlock[0], 1) < 0) err_sys("can't unlock");
}

void sem_op(int id, int value)
{
  if((op_op[0].sem_op = value) == 0) err_sys("can't have value == 0");
  while(semop(id, &op_op[0], 1) < 0) err_sys("sem_op error");
}

void sem_wait(int id)
{
  sem_op(id, -1);
}

void sem_signal(int id)
{
  sem_op(id, 1);
}

void lock(int semid)
{
  sem_wait(semid);
}

void unlock(int semid)
{
  sem_signal(semid);
}




#endif
