#include <map>
#include <string>

#include "Client.cpp"

using namespace std;

class ClientManager
{
public:
  ClientManager();
  // virtual ~ClientManager();
  int getNewID();
  int addNewClient(int, string, int);
  void removeClient(int);
  int clientIDExist(int);
  Client* getClientByID(int);
  Client* getClientByFD(int);
  string getWhoIsOnline(int);
  vector<int> getAllFDs();
  map<int, Client> getAllClients();
  int checkUniqueName(string);

private:
  map<int, Client> _clients;
};
