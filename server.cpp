#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>

#include "shell.cpp"
#include "sockFunc.cpp"
#include "sharedMemory.cpp"
#include "semaphoreFunc.cpp"

#define SHMSEMKEY 12345

using namespace std;

void clientHandler(int, int);
int commandHandler(int, int);
int name(int, int, string);
int tell(int, int, string);
void yell(int, string);
void broadcastMessage(string);
void sendConnectMsg(string, int);
void initSemaphore();

int clientID;
int clientFD;
int shmid = -1;
int sharedMemoryLock = -1;

// handle zombie process.
int reaper()
{
  union wait status;
  while(wait3(&status, WNOHANG, (struct rusage*)0) >= 0)
    ;
}

int freeSharedMemory()
{
  shmdt(clientManager);
  shmctl(shmid, IPC_RMID, 0);
  sem_rm(sharedMemoryLock);
  exit(1);
}

int receiveMsg()
{
  client* c = &clientManager[clientID-1];

  int i;
  for (i = 0; i < URDMSGMAX; ++i) {
    if(strcmp(c->msg[i], "") != 0){
      write(clientFD, c->msg[i], strlen(c->msg[i]));
      strcpy(c->msg[i], "");
    }
  }
}

int main(int argc, char* argv[], char* envp[]){

  char* port = (char*) "2015";
  if (argv[1] != NULL){
    port = argv[1];
  }


  struct sockaddr_in fsin;
  int alen;

  // int fd = createSocket(port);
  int msock = passiveTCP(port, 40);
  int ssock;


  clearenv();
  setenv("PATH", "bin:.", 1);
  // change current direcotory to ras
  chdir("./ras");

  shmid = initSharedMemory();
  initSemaphore();


  int id;

  while(1){
    // ssock = acceptConnection(msock);
    ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
    cout << "accept\n";

    if(ssock < 0){
      cerr << "ssock error: " << to_string(errno) << endl; 
      exit(0);
    }

    // executeShell(ssock);
    switch (fork()) {
      case 0:
        // children process
        close(msock);
        id = addClient(getpid(), fsin.sin_port, inet_ntoa(fsin.sin_addr));
        clientID = id;
        clientFD = ssock;

        // add signal for kill zombie process.
        (void) signal(SIGCHLD, (__sighandler_t)reaper);
        // add signal for receive msg in shared memory
        (void) signal(SIGUSR1, (__sighandler_t)receiveMsg);

        // send welcome message
        sendWelcomeMSG(ssock);
        sendConnectMsg(string(inet_ntoa(fsin.sin_addr)), fsin.sin_port);
        // handle user input.
        clientHandler(ssock, id);
        exit(1);
      default:
        (void) signal(SIGINT, (__sighandler_t)freeSharedMemory);
        // parent proces
        close(ssock);
        break;
      case -1:
        exit(0);
    }
  }
}

void initSemaphore()
{
  if(sharedMemoryLock < 0){
    if((sharedMemoryLock = sem_create(SHMSEMKEY, 1)) < 0)
      err_sys("sem_create error");
  }
}


void sendConnectMsg(string ip, int port)
{
  string connectMSG = "*** User '(No Name)' entered from " + ip + "/" + to_string(port)  + ". ***\n";
  broadcastMessage(connectMSG);
}

int commandHandler(int clientfd, int id)
{
  Utility utility = Utility();
  string inputString = readLineFromSocket(clientfd);

  // remove break line in input string.
  inputString = utility.removeLineBreak(inputString);
  int commandType = utility.checkCommandType(inputString);

  string outputString;
  int result;
  switch(commandType){
    // who
    case 1:
      lock(sharedMemoryLock);
      outputString = who(id);
      unlock(sharedMemoryLock);
      write(clientfd, outputString.c_str(), outputString.size());
      pipeHandler.countDownPipes();
      return 0;
    // tell
    case 2:
      lock(sharedMemoryLock);
      result = tell(id, clientfd, inputString);
      unlock(sharedMemoryLock);
      if(result){
        pipeHandler.countDownPipes();
      }
      return 0;
    // yell
    case 3:
      lock(sharedMemoryLock);
      yell(id, inputString);
      unlock(sharedMemoryLock);
      pipeHandler.countDownPipes();
      return 0;
    // name
    case 4:
      lock(sharedMemoryLock);
      result = name(id, clientfd, inputString);
      unlock(sharedMemoryLock);
      if(result){
        pipeHandler.countDownPipes();
      }
      return 0;
    case -1: 
      return 0;
  }
  return handleCommand(id, string(clientManager[id-1].name), inputString, clientfd, &broadcastMessage);
}

int tell(int id, int clientfd, string inputString)
{
  regex re("\\w+\\s+(\\d+)\\s+(.*)$");
  smatch sm;

  // get match string by regular expression.
  regex_match(inputString, sm, re); 

  int targetID = stoi(sm[1]);
  string inputSentence = sm[2];
  string size = to_string(sm.size());

  client* c = &clientManager[id-1];
  string msg = "*** " + string(c->name) + " told you ***: " + inputSentence + "\n";

  client* tarClient = &clientManager[targetID-1];
  if(tarClient->id != -1){
    writeUnreadMsg(targetID, msg);
    return 1;
  } else {
    msg = "*** Error: user #" + to_string(targetID)+ " does not exist yet. ***\n";
    write(clientfd, msg.c_str(), msg.size());
    return 0; 
  }
}

void broadcastMessage(string msg)
{
  int i;
  for (i = 0; i < MAXCLIENT; ++i) {
    client* currClient = &clientManager[i];
    if(currClient->id != -1)
      writeUnreadMsg(currClient->id, msg);
  }
}

void yell(int id, string inputString)
{
  regex re("\\w+\\s+(.*)$");
  smatch sm;

  // get match string by regular expression.
  regex_match(inputString, sm, re); 

  string inputSentence = sm[1];
  string msg = "*** " + string(clientManager[id-1].name) + " yelled ***: " + inputSentence + "\n";
  broadcastMessage(msg);

}

int name(int id, int clientfd, string inputString)
{

  regex re("\\w+\\s+(.*)");
  smatch sm;

  // get match string by regular expression.
  regex_match(inputString, sm, re); 

  string wantedName = sm[1];
  string msg;
  client* c = &clientManager[id-1];

  if(changeName(id, wantedName.c_str())){
    msg = "*** User from " + string(c->ip) + "/" + to_string(c->port) + " is named '" + string(c->name)  + "'. ***\n";
    // write(clientfd, msg.c_str(), msg.size());
    broadcastMessage(msg);
    return 1;
  } else {
    msg = "*** User '" + wantedName +"' already exists. ***\n";
    write(clientfd, msg.c_str(), msg.size());
    return 0;
  }
  
}

void clientHandler(int ssock, int id)
{
  initShell();
  write(ssock, PROMOTE_MSG.c_str(), PROMOTE_MSG.size());

  // handle user input while user does not enter 'exit'
  while(!commandHandler(ssock, id)){
    write(ssock, PROMOTE_MSG.c_str(), PROMOTE_MSG.size());
  }


  // broadcastMessage exit message
  client* c = &clientManager[id-1];
  string exitMsg = "*** User '" + string(c->name) + "' left. ***\n";
  broadcastMessage(exitMsg);
  // remove client and exit.
  removeClient(id);
  exit(1);
}

