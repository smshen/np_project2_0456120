#ifndef PIPEHANDLER_H
#define PIPEHANDLER_H

#include "PipeHandler.h"
#include <vector>
#include <iostream>
#include <unistd.h>

using namespace std;

PipeHandler::PipeHandler(){
}

PipeHandler::~PipeHandler(){
}


// check if exist a pipe can read.
int PipeHandler::existReadFd()
{
  for(vector<Pipe>::iterator it = storePipes.begin(); it!=storePipes.end();){
    if(it->count() == 0){
      it->countDown();
      return it->readfd();
    } else {
      ++it;
    }
  }
  return STDIN_FILENO;
}

// check if exist a pipe write to same destination.
int PipeHandler::existWriteFd(int count)
{
  int i;
  // if exist a write pipe has same destination, return this pipe.
  for (i = 0; i < storePipes.size(); ++i) {
    Pipe currPipe = storePipes[i];
    if(currPipe.count() == count)
      return currPipe.writefd();
  }

  // if not exist, create a new pipe and store it.
  int pipefd[2];
  pipe(pipefd);
  storePipes.push_back(Pipe(pipefd[0], pipefd[1], count));

  return pipefd[1];
}

// close useless pipe.
void PipeHandler::closePipes()
{
  // iterate all exist pipes.
  for(vector<Pipe>::iterator it = storePipes.begin(); it!=storePipes.end();){
    // if count is 0 means the pipe has been written by all processes.
    if(it->count() == 0){
      close(it->writefd());
      // if the pipe has read.
      ++it;
    // if count is -1 means the pipe has been read, then the pipe is useless, so remove it.
    } else if(it->count() == -1){
      close(it->readfd());
      storePipes.erase(it);
    } else {
      ++it;
    }
  }
}

// count down the count of exist pipes.
void PipeHandler::countDownPipes()
{
  for(vector<Pipe>::iterator it = storePipes.begin(); it!=storePipes.end(); ++it){
    if(it->count() != 0){
      it->countDown();
    }
    if(it->count() == 0){
      close(it->writefd());
    } 
    // cout << "pipes: "<< it->readfd() << "," << it->writefd() << it->count() << endl;
  }
}
void PipeHandler::showPipes()
{
  int i;
  for (i = 0; i < storePipes.size(); ++i) {
    cout << "Pipe" << i
      << "\nread:  " << storePipes[i].readfd()
      << "\nwrite: " << storePipes[i].writefd()
      << "\ncount: " << storePipes[i].count()
      << "\n----------------\n";
  }
}
#endif
