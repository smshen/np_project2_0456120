#ifndef SHAREDMEMORY_H
#define SHAREDMEMORY_H

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string>
#include <signal.h>

#define SHMKEY ((key_t) 7890)
#define PERMS 0666
#define MAXCLIENT 30		//Maximum connection queue length.
#define URDMSGMAX 10
#define MSGMAX 1024

using namespace std;

typedef struct client{
	int id;
	int pid;
	int port;
	char ip[17];
	char name[21];
	char msg[URDMSGMAX][MSGMAX];
} client;

client* clientManager;

int initSharedMemory();
void initClientManager();
void initClient(client*, int, int, int, char*);
int addClient(int, int, char*);
void removeClient(int);
string who(int);
int writeUnreadMsg(int, string);


// tell to client
int writeUnreadMsg(int id, string msg)
{
  client* targetClient = &clientManager[id-1];
  
  if(targetClient->id == -1)
    return 0;

  int i;
  for (i = 0; i < URDMSGMAX ; ++i) {
    if(!strcmp(targetClient->msg[i], "")){
      strcpy(targetClient->msg[i], msg.c_str());
      kill(targetClient->pid, SIGUSR1);
      return 1;
    }
  }
  return 0;
}


int initSharedMemory()
{
  int shmid;
  if ((shmid = shmget(SHMKEY, (sizeof(client)*MAXCLIENT), PERMS | IPC_CREAT)) < 0){
    cerr << "shget error\n";
    exit(0);
  }
  if ((clientManager = (client *)shmat(shmid, (char *)0, 0)) == (client *)-1){
    cerr << "shmat error\n";
    exit(0);
  }

  // initialize client manager.
  initClientManager();
  return shmid;
}

void initClient(client* client, int id, int pid, int port, char* ip)
{
  client->id = id;
  client->pid = pid;
  client->port = port;
  strcpy(client->ip, ip);
  strcpy(client->name, (char *) "(no name)\0");

  int i;
  for (i = 0; i < URDMSGMAX; ++i) {
    strcpy(client->msg[i], "");
  }
}

void initClientManager()
{
  int i;
  for (i = 0; i < MAXCLIENT; ++i) {
    initClient(&clientManager[i], -1, -1, -1, (char *)"");
  }
}


// name instruction for change client name
int changeName(int id, const char* name)
{
  int i;
  for (i = 0; i < MAXCLIENT; ++i) {
    if(strcmp(clientManager[i].name, name) == 0)
      return 0;
  }

  client* currClient = &clientManager[id-1];
  strcpy(currClient->name, name);
  return 1;
}

int addClient(int pid, int port, char* ip)
{
  int i;
  for (i = 0; i < MAXCLIENT; ++i) {
    client* c = &clientManager[i];
    if(c->id == -1){
      int id = i+1;
      initClient(c, id, pid, port, ip);
      return id;
    }
  }
  return -1;
}

void removeClient(int id)
{
  client* c = &clientManager[id-1];
  initClient(c, -1, -1, -1, (char *)"");
}

// get information string of online  users.
string who(int id)
{
  string infoString = "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n";
  int i;
  for (i = 0; i < MAXCLIENT; ++i) {
    client c = clientManager[i];
    if(c.id != -1){
      infoString += to_string(c.id) + "\t";
      infoString += string(c.name) + "\t";
      infoString += string(c.ip) + "/" + to_string(c.port) + "\t";
      if(c.id == (id))
        infoString += "<-me";
      infoString += "\n";
    }
  }
  return infoString;
}

#endif
