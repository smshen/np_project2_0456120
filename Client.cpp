#ifndef CLIENT_H
#define CLIENT_H
#include <string>

#include "Client.h"

using namespace std;

Client::Client(int clientID, int clientFD, string clientIP, int clientPort){

  _clientID    = clientID;
  _clientFD    = clientFD;
  _clientIP    = clientIP;
  _clientPort  = clientPort;
  _name        = "(No Name)";
  _env         = "bin:.";
  _pipeHandler = PipeHandler();

}

int Client::getID(){
  return _clientID;
}

int Client::getFD(){
  return _clientFD;
}

string Client::getIP(){
  return _clientIP;
}

int Client::getPort(){
  return _clientPort;
}

void Client::setEnv(string env){
  _env = env;
}

string Client::getEnv(){
  return _env;
}

void Client::setName(string name){
  _name = name;
}

string Client::getName(){
  return _name;
}

void Client::setPipeHandler(PipeHandler pipeHandler){
  _pipeHandler = pipeHandler;
}

PipeHandler Client::getPipeHandler(){
  return _pipeHandler;
}
#endif
