
#include "Pipe.cpp"
#include <vector>

using namespace std;

class PipeHandler
{
public:
  PipeHandler ();
  virtual ~PipeHandler ();
  int existReadFd();
  int existWriteFd(int);
  void closePipes();
  void countDownPipes();
  void showPipes();
private:
  vector<Pipe> storePipes;
};
