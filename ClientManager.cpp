#ifndef CLIENTHANDLER_H
#define CLIENTHANDLER_H

#include "ClientManager.h"

using namespace std;

ClientManager::ClientManager(){
}

int ClientManager::getNewID()
{
  int i;
  // if clients map is empty, return id 1.
  if(_clients.size() == 0)
    return 1;

  // if map exist missing number, return the missing number.
  for (i = 0; i < _clients.size(); ++i) {
    if(_clients.find(i+1) == _clients.end())
      return i+1;
  }

  // if no missing number in map, return new id.
  return i+1;
}

int ClientManager::addNewClient(int clientFD, string clientIP, int clientPort)
{
  int clientID = getNewID();
  Client newClient = Client(clientID, clientFD, clientIP, clientPort);
  _clients.insert(make_pair(clientID, newClient));
  return clientID;
}

void ClientManager::removeClient(int id)
{
  _clients.erase(_clients.find(id));
}

int ClientManager::clientIDExist(int id)
{
  map<int, Client>::iterator it = _clients.find(id);
  return it != _clients.end();
}

Client* ClientManager::getClientByID(int id)
{
  return &(_clients.find(id)->second);
}

Client* ClientManager::getClientByFD(int fd)
{
  
  map<int, Client>::iterator it;
  for(it = _clients.begin() ; it != _clients.end() ; ++it){
    Client* client = &(it->second);
    if(client->getFD() == fd)
      return client;
  }

}

string ClientManager::getWhoIsOnline(int fd)
{
  map<int, Client>::iterator it;
  string infoString = "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n";
  for(it = _clients.begin() ; it != _clients.end() ; ++it){
    Client client = it->second;
    infoString += to_string(client.getID()) + "\t";
    infoString += client.getName() + "\t";
    infoString += client.getIP() + "/" + to_string(client.getPort()) + "\t";

    if(client.getFD() == fd)
      infoString += "<-me";
    infoString += "\n";
  }
  return infoString;
}

vector<int> ClientManager::getAllFDs()
{
  
  vector<int> fds;
  map<int, Client>::iterator it;
  for(it = _clients.begin() ; it != _clients.end() ; ++it){
    Client* client = &(it->second);
    fds.push_back(client->getFD());
  }

  return fds;
}

int ClientManager::checkUniqueName(string name)
{

  map<int, Client>::iterator it;
  for(it = _clients.begin() ; it != _clients.end() ; ++it){
    Client* client = &(it->second);
    if(client->getName() == name)
      return 0;
  }
  return 1; 
}

map<int, Client> ClientManager::getAllClients()
{
  return _clients;
}


#endif
