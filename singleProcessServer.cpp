#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <regex>

#include "ClientManager.cpp"
#include "sockFunc.cpp"
#include "shell.cpp"
#include "Utility.cpp"

#define QLEN 40
#define BUFSIZE 4096

using namespace std;

int commandHandler(int);
int name(string);
void broadcastMessage(string);
void yell(Client*, string);
int tell(Client*, string);
void acceptMSG(int, string, int);
void setupEnv(Client*);
void storeEnv(Client*);

ClientManager clientManager;

int main(int argc, char *argv[])
{
  struct sockaddr_in fsin;
  int msock;
  fd_set rfds;
  fd_set afds;
  int alen;
  int fd, nfds;
  char* port = (char*) "2015";

  if (argv[1] != NULL){
    port = argv[1];
  }
  
  clearenv();
  setenv("PATH", "bin:.", 1);
  // change current direcotory to ras
  chdir("./ras");

  initShell();

  // create a socket
  msock = passiveTCP(port, QLEN);
  nfds = getdtablesize();
  FD_ZERO(&afds);
  FD_SET(msock, &afds);

  while(1){
    memcpy(&rfds, &afds, sizeof(rfds));

    if(select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0){
      cerr << "Select error...\n";
      exit(0);
    }

    if(FD_ISSET(msock, &rfds)){
      int ssock;
      alen = sizeof(fsin);
      ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
      cout << "accpet\n";
      // cerr << ssock << " port: " << fsin.sin_port << " IP: " << inet_ntoa(fsin.sin_addr);
      clientManager.addNewClient(ssock, string(inet_ntoa(fsin.sin_addr)), fsin.sin_port);
      acceptMSG(ssock, string(inet_ntoa(fsin.sin_addr)), fsin.sin_port);

      if(ssock < 0){
        cerr << "Accept error...\n";
        exit(0);
      }
      FD_SET(ssock, &afds);
    }
    for (fd = 0; fd < nfds; ++fd) {
      if (fd!=msock && FD_ISSET(fd, &rfds)) {

        Client* currClient = clientManager.getClientByFD(fd);
        setupEnv(currClient);

        // if client exit normally, remove client and close fd
        if(commandHandler(fd)){
          broadcastMessage("*** User '" + currClient->getName() + "' left. ***\n");
          clientManager.removeClient(currClient->getID());
          close(fd);
          FD_CLR(fd, &afds);
        } else {
          write(fd, PROMOTE_MSG.c_str(), PROMOTE_MSG.size());
          // else store status of this client.
          storeEnv(currClient);
        }
      }
    }
  }

  return 0;
}

void acceptMSG(int fd, string ip, int port)
{
  string connectMSG = "*** User '(no name)' entered from " + ip + "/" + to_string(port)  + ". ***\n";
  sendWelcomeMSG(fd);
  // broadcast message to all clients.
  broadcastMessage(connectMSG);
  write(fd, PROMOTE_MSG.c_str(), PROMOTE_MSG.size());
  
}


// broadcast message to all client in clientManager.
void broadcastMessage(string msg)
{
  broadcast(msg, clientManager.getAllFDs());
}

int name(Client* client, string instString)
{
  regex re("\\w+\\s+(.*)");
  smatch sm;

  // get match string by regular expression.
  regex_match(instString, sm, re); 

  string wantedName = sm[1];
  string msg;

  // if the name is valid, broadcast to all users.
  if(clientManager.checkUniqueName(wantedName)){
    msg = "*** User from " + client->getIP() + "/" + to_string(client->getPort()) + " is named '" + wantedName  + "'. ***\n";
    client->setName(wantedName);
    // write(client->getFD(), msg.c_str(), msg.size());
    broadcastMessage(msg);
    return 1;
  // else if the name is invalid, send error msg to current user.
  } else {
    msg = "*** User '" + wantedName +"' already exists. ***\n";
    write(client->getFD(), msg.c_str(), msg.size());
    return 0;
  }
}

void yell(Client* client, string inputString)
{

  regex re("\\w+\\s+(.*)$");
  smatch sm;

  // get match string by regular expression.
  regex_match(inputString, sm, re); 

  string inputSentence = sm[1];
  string msg = "*** " + client->getName() + " yelled ***: " + inputSentence + "\n";


  broadcastMessage(msg);
}

int tell(Client* client, string inputString)
{
  regex re("\\w+\\s+(\\d+)\\s+(.*)$");
  smatch sm;

  // get match string by regular expression.
  regex_match(inputString, sm, re); 

  int targetID = stoi(sm[1]);
  string inputSentence = sm[2];
  string size = to_string(sm.size());

  string msg;

  if(clientManager.clientIDExist(targetID)){
    // if target client exist, send the msg to target client.
    Client* tarClient = clientManager.getClientByID(targetID);
    msg = "*** " + client->getName() + " told you ***: " + inputSentence + "\n";
    write(tarClient->getFD(), msg.c_str(), msg.size());
    return 1;
  } else {
    // if target client does not exist, send error message to client.
    msg = "*** Error: user #" + to_string(targetID)+ " does not exist yet. ***\n";
    write(client->getFD(), msg.c_str(), msg.size());
    return 0;
  }
}


// return 1, if process is finished
int commandHandler(int clientfd)
{
  Utility utility = Utility();
  string inputString = readLineFromSocket(clientfd);

  // remove break line in input string.
  inputString = utility.removeLineBreak(inputString);
  int commandType = utility.checkCommandType(inputString);

  
  Client* currClient = clientManager.getClientByFD(clientfd);
  string outputString;
  int result;
  switch(commandType){
    // who
    case 1:
      outputString = clientManager.getWhoIsOnline(clientfd);
      write(clientfd, outputString.c_str(), outputString.size());
      pipeHandler.countDownPipes();
      return 0;
    // tell
    case 2:
      result = tell(currClient, inputString);
      if(result){
        pipeHandler.countDownPipes();
      }
      return 0;
    // yell
    case 3:
      yell(currClient, inputString);
      pipeHandler.countDownPipes();
      return 0;
    // name
    case 4:
      result = name(currClient, inputString);
      if(result){
        pipeHandler.countDownPipes();
      }
      return 0;
    case -1: 
      return 0;
  }
  return handleCommand(currClient->getID(), currClient->getName(), inputString, clientfd, &broadcastMessage);
}

// initialize envionment and pipehandler when user switch.
void setupEnv(Client* client)
{
  pipeHandler = client->getPipeHandler();
  setenv("PATH", client->getEnv().c_str(), 1);
  updateValidInstruction();
  utility = Utility();
}

void storeEnv(Client* client)
{
  client->setPipeHandler(pipeHandler);
  client->setEnv(getenv("PATH"));
  updateValidInstruction();
}


