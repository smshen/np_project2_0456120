#ifndef SHELL_H
#define SHELL_H
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <sstream>
#include <sys/types.h>
#include <sys/wait.h>
#include <map>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <regex>

// #include "Pipe.cpp"
#include "PipeHandler.cpp"
#include "Utility.cpp"
#include "sockFunc.cpp"
#include "publicPipe.cpp"

#include "semaphoreFunc.cpp"

#define FIFOSEMKEY 5555

using namespace std;

PipeHandler pipeHandler;
Utility utility;
vector<string> legalInstVector;
int fifoLock = -1;

string WELCOME_MSG = "****************************************\n** Welcome to the information server. **\n****************************************\n";
string PROMOTE_MSG = "% ";



// update the legal instruction after setenv
void updateValidInstruction()
{
  // clear old instruction
  legalInstVector.clear();

  vector<string> envPathVector = utility.split(getenv("PATH"), ':');
  int i;
  // iterate all directory path in environment.
  for (i = 0; i < envPathVector.size(); ++i) {
    DIR* dir = opendir(envPathVector[i].c_str());
    struct dirent *ent;

    // store all file in direcotory
    while ((ent = readdir (dir)) != NULL) {
      legalInstVector.push_back(ent->d_name);
    }
    closedir(dir);
  }
}
void initShell()
{
  setenv("PATH", "bin:.", 1);
  updateValidInstruction();
  pipeHandler = PipeHandler();
  utility = Utility();
  // create semaphore for FIFO
  if((fifoLock = sem_create(FIFOSEMKEY, 1)) < 0)
    err_sys("FIFO sem_create error");
}


void dupTable(int inputfd, int outputfd ,int errfd, string writeFileName)
{
  // cout << inputfd << "," << outputfd <<"," << errfd <<endl;
  dup2(inputfd, STDIN_FILENO);
  if(writeFileName == ""){
    dup2(outputfd, STDOUT_FILENO);
  } else {
    int fd;
    fd = open(writeFileName.c_str(), O_WRONLY|O_CREAT, 0777);
    dup2(fd, STDOUT_FILENO);
  }

  dup2(errfd, STDERR_FILENO);

  // close the fd if it's not standard.
  if(inputfd != STDIN_FILENO)
    close(inputfd);
  if(outputfd != STDOUT_FILENO)
    close(outputfd);
  if(errfd != STDERR_FILENO)
    close(errfd);
}


int executeInstruction(int clientfd, string instructionName, string option, int inputfd, int outputfd ,int errfd, string writeFileName, int id, string clientName, string command, void (*broadcastToAllClients)(string))
{

  // convert option string to char* [] for execvp.
  vector<string> optionVector = utility.split(option, ' ');
  vector<char*> argv(optionVector.size() + 1);
  for (size_t i = 0; i != optionVector.size(); ++i){
    argv[i] = &optionVector[i][0];
  }

  string msg;
  // read from public pipe.
  string publicInputFIFO = "";
  if(inputfd < 0){

    int readPipe = inputfd * -1;
    // store public pipe path
    publicInputFIFO = "../" + to_string(readPipe);

    if((inputfd = readFIFO(publicInputFIFO.c_str())) < 0){
      msg = "*** Error: public pipe #" + to_string(readPipe) + " does not exist yet. ***\n";
      write(clientfd, msg.c_str(), msg.size());
    } else {
      msg = "*** " + clientName + " (#" + to_string(id) + ") just received via '" + command + "' ***\n";
      broadcastToAllClients(msg);
    }
  }

  int originOutputfd = outputfd;

  // write to public pipe.
  if(outputfd < 0){
    int writePipe = outputfd * -1;

    string pipePath = "../" + to_string(writePipe);

    if((outputfd = writeFIFO(pipePath.c_str())) < 0){
      msg = "*** Error: public pipe " + to_string(writePipe) + " already exists. ***\n";
      write(clientfd, msg.c_str(), msg.size());
    } else {
      msg = "*** " + clientName +  " (#" + to_string(id) + ") just piped '" + command + "' ***\n";
      broadcastToAllClients(msg);
      errfd = outputfd;
    }
  }

  // setenv
  if(instructionName == "setenv") {
    setenv(argv[1], argv[2], 1);
    updateValidInstruction();
    return 1;
  }

  if(instructionName == "exit"){
    // exit(1);
    return -1;
  }

  int pid = fork();
  if(pid == 0){
    // children process

    // if public pipe has error, terminate.
    if(inputfd < 0 || outputfd < 0)
      exit(0);

    dupTable(inputfd, outputfd ,errfd, writeFileName);

    if(instructionName == "printenv") {
      cout << optionVector[1] << "=" << getenv(optionVector[1].c_str()) << endl;
      exit(1);
    }

    int exeResult = execvp(instructionName.c_str(), argv.data());
    if(exeResult == -1){
      cerr << "Unknown command: [" << instructionName << "].\n";
    }
    exit(0);
  } else {
    // parent process
    pipeHandler.closePipes();

    int status;
    wait(&status);

    // if inputfd is from FIFO, remove the FIFO.
    if(publicInputFIFO != ""){
      close(inputfd);
      unlink(publicInputFIFO.c_str());
    }

    if(originOutputfd < 0)
      close(outputfd);
  }
  
  if(inputfd < 0 || outputfd < 0)
    return 0;

  return 1;
}



// check the instruction is valid or not in the environment varaible
int checkLegalInstruction(string instruction)
{
  // get first instruction
  vector<string> commandVector;
  commandVector = utility.split(instruction, '|');
  
  // get word in first instruction.
  vector<string> words = utility.splitInstruction(commandVector[0]);

  // get first instruction name.
  instruction = words[0];

  int i;
  // if public pipe operator has error, return 0.
  for (i = 0; i < words.size(); ++i) {
    string word = words[i];
    string pipePath;
    if(word[0] == '<' && word.size() > 1){
      pipePath = "../" + word.substr(1);
      if(!isFIFOExist(pipePath.c_str()))
        return 0;
    }
    if(word[0] == '>' && word.size() > 1){
      pipePath = "../" + word.substr(1);
      if(isFIFOExist(pipePath.c_str()))
        return 0;
    }
  }
  

  // if instruction is valid, return 1.
  for (i = 0; i < legalInstVector.size(); ++i) {
    if(instruction == legalInstVector[i])
      return 1;
  }
  return 0;
}

// handle the received command 
int handleCommand(int id, string clientName, string commandString, int clientfd, void (*broadcastToAllClients)(string))
{

  lock(fifoLock);
  vector<string> inputWordVector;
  int finished = 0;

  // parse the command into instructions.
  commandString = utility.removeLineBreak(commandString);
  inputWordVector = utility.splitInstruction(commandString);

  int linePass = 0;
  for(int i=0;i<inputWordVector.size();i++){
    if(linePass)
      break;
    string instructionName = inputWordVector[i];

    // if first instruction is legal, count down pipes.
    if(i==0 && checkLegalInstruction(commandString)){
      pipeHandler.countDownPipes();
    }

    // pipeHandler.showPipes();

    // handle exit instruction.
    if(instructionName == "exit"){
      finished = 1;
      break;
    }

    // add instruction name to option for argument of execlp.
    string option = instructionName;
    string nextWord = instructionName;

    // get the remaining options of instruction
    while (i != (inputWordVector.size() - 1)) {
      nextWord = inputWordVector[++i];
      // if next word is a option of instruction
      if(utility.checkInputType(nextWord) == 0){
        option += " " + nextWord;
      } else {
        break;
      }
    }

    int inputfd  = STDIN_FILENO;
    int outputfd = clientfd;
    int errfd    = clientfd;
    string writeFileName = "";

    // check instruction type.
    do{
      int type = utility.checkInputType(nextWord);
      int number = 0;
      // check if exist a pipe need to read.
      if(inputfd == STDIN_FILENO){
        inputfd = pipeHandler.existReadFd();
      }
      // if current instruction is illegal, ignore all instructions behind it.
      if(!checkLegalInstruction(instructionName)){
        linePass = 1;
        // break;
      }
      switch(type) {
        case 0:
          outputfd = clientfd;
          break;
          // case '|'
        case 1:
          number = utility.checkOperatorNumber(nextWord);
          outputfd = pipeHandler.existWriteFd(number);
          break;
          // case '!'
        case 2:
          number = utility.checkOperatorNumber(nextWord);
          errfd = pipeHandler.existWriteFd(number);
          break;
          // case '>'
        case 3:
          number = utility.checkOperatorNumber(nextWord);
          nextWord = inputWordVector[++i];
          writeFileName = nextWord;
          break;
        // case '<n'
        case 4:
          number = utility.checkOperatorNumber(nextWord);
          inputfd = number * -1;
          break;
        // case '>n'
        case 5:
          number = utility.checkOperatorNumber(nextWord);
          outputfd = number * -1;
          break;

      }
      // if exist next instruction
      if(i != (inputWordVector.size() - 1)){
        nextWord = inputWordVector[++i];
        // if next word is a instruction reseek it.
        if(utility.checkInputType(nextWord) == 0){
          --i;
          break;
        }
        // if reach the end of this command, break.
      } else {
        break;
      }
    } while (1);
    executeInstruction(clientfd, instructionName, option, inputfd, outputfd, errfd, writeFileName, id, clientName, commandString, broadcastToAllClients);
  }
  unlock(fifoLock);
  return finished;
}

void sendWelcomeMSG(int clientfd)
{
  write(clientfd, WELCOME_MSG.c_str(), WELCOME_MSG.size());
}

#endif
