#ifndef SOCKFUNC_H
#define SOCKFUNC_H

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <netdb.h>
#include <stdio.h>

#include "sockFunc.h"


int portbase = 0;
int MAX_LENGTH_PER_CMD_LINE = 15000;

// create socket
int passivesock(char* service, const char* protocol, int qlen)
{
  struct servent *pse;
  struct protoent *ppe;
  struct sockaddr_in sin;
  int s, type;

  bzero((char*)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;

  /* Map service name to port name*/
  if(pse = getservbyname(service, protocol))
    sin.sin_port = htons(ntohs((u_short)pse->s_port) + portbase);
  else if((sin.sin_port = htons((u_short)atoi(service))) == 0){
    exit(0);
  }

  /* Map protocol name to protocol number */
  if((ppe = getprotobyname(protocol)) == 0){
    exit(0);
  }

  /* Use protocol to choose a socket type */
  if(strcmp(protocol, "udp") == 0)
    type = SOCK_DGRAM;
  else
    type = SOCK_STREAM;

  /* Allocate a socket*/
  s = socket(PF_INET, type, ppe->p_proto);
  if(s < 0){
    cerr <<  "Socket error\n";
    exit(0);
  }

  /* Bind the socket */
  if(bind(s, (struct sockaddr*)&sin, sizeof(sin)) < 0){
    cerr << "Bind error\n";
    exit(0);
  }
  if(type == SOCK_STREAM && listen(s, qlen) < 0){
    cerr << "Listen error\n";
    exit(0);
  }
  return s;

}

// create a TCP socket
int passiveTCP(char* service, int qlen)
{
  return passivesock(service, "tcp", qlen);
}

int acceptConnection(int sockfd){
  struct sockaddr_in cli_addr;
  socklen_t clilen;
  clilen = sizeof(cli_addr);

  int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0){
    cerr << "Accept Error" << endl;
    exit(0);
  }

  return newsockfd;
}

char* readLineFromSocket(int newfd){
  int n         = -1;
  bool endofcmd = false;

  char buffer[MAX_LENGTH_PER_CMD_LINE];
  char* cmds = new char[MAX_LENGTH_PER_CMD_LINE];

  bzero(cmds, MAX_LENGTH_PER_CMD_LINE);
  while(!endofcmd){
    bzero(buffer, MAX_LENGTH_PER_CMD_LINE);
    if ((n =read(newfd, buffer, MAX_LENGTH_PER_CMD_LINE)) < 0) {
      cerr << "Read Error" << endl;
    }
    else{
      strcat(cmds, buffer);
      if(buffer[n-1] == '\n'){
        endofcmd = true;
      }
    }
  } 
  return cmds;
}

void broadcast(string msg, vector<int> fds)
{
  int i;
  for (i = 0; i < fds.size(); ++i) {
    write(fds[i], msg.c_str(), msg.size());
  }
}

#endif
