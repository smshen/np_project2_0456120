#include <vector>
#include <string>

using namespace std;

class Utility
{
public:
  Utility ();
  virtual ~Utility ();

  vector<string> split(string, char);
  string removeLineBreak(string);
  vector<string> splitInstruction(string);
  int checkInputType(string);
  int checkOperatorNumber(string);
  int checkCommandType(string);
  string trim(string);

private:
  /* data */
};
