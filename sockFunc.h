#include <string>
#include <vector>

using namespace std;

int passivesock(char*, const char*, int);
int passiveTCP(char*, int);
int acceptConnection(int);
char* readLineFromSocket(int);
void broadcast(string, vector<int>);
