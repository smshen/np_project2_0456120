#ifndef PIPE_H
#define PIPE_H

class Pipe
{
public:
  Pipe (int readfd, int writefd, int count){
    _readfd  = readfd;
    _writefd = writefd;
    _count   = count;
  }
  // virtual ~Pipe ();
  int readfd(){
    return _readfd;
  }

  void readfd(int readfd){
    _readfd = readfd;
  }

  int writefd(){
    return _writefd;
  }

  int count(){
    return _count;
  }

  void countDown(){
    _count--;
  }

private:
  /* data */
  int _readfd;
  int _writefd;
  int _count;
};
#endif
