#include <string>
#include "PipeHandler.cpp"

using namespace std;

class Client
{
public:
  Client (int, int, string, int);
  // virtual ~Client ();
  int getID();
  int getFD();
  string getIP();
  int getPort();
  void setName(string name);
  string getName();
  void setEnv(string env);
  string getEnv();
  void setPipeHandler(PipeHandler);
  PipeHandler getPipeHandler();

private:
  int _clientID;
  int _clientFD;
  string _clientIP;
  int _clientPort;
  string _name;
  string _env;
  PipeHandler _pipeHandler;
};
