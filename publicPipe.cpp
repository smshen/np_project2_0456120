#ifndef PUBLICPIPE_H
#define PUBLICPIPE_H

#define PERMS 0666

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <iostream>
#include <sys/wait.h>


#include "sockFunc.cpp"

// check FIFO exist or not.
int isFIFOExist(const char* pipeName){
  struct stat st;
  // if the fifo exist, return 1, else return 0;
  if(stat(pipeName, &st) == 0)
    return 1;
  return 0;
}

int readFIFO(const char* pipeName)
{
  struct stat st;
  // if the fifo does not exist, return -1
  if(stat(pipeName, &st) != 0)
    return -1;

  int fifoFD = open(pipeName, O_NONBLOCK | O_RDONLY);
  return fifoFD;
}

int writeFIFO(const char* pipeName)
{
  struct stat st;
  // if the fifo exist, return -1
  if(stat(pipeName, &st) == 0)
    return -1;
  if((mknod(pipeName, S_IFIFO | PERMS, 0) < 0) && (errno != EEXIST))
    return -1;

  int fifoFD = open(pipeName, O_RDWR);
  readFIFO(pipeName);
  return fifoFD;
}


#endif
